<?php

namespace src;

use Luracast\Restler\RestException;
use Exception;

class Contacts
{
	public function index()
	{
		try {
			$result = ContactsHelper::getAll();
		}
		catch(Exception $e){
			throw new RestException(500, $e->getMessage());
		}

		return $result;
	}

	public function get($id)
	{
		try {
			$result = ContactsHelper::getOne($id);
		} catch (Exception $e) {
			throw new RestException(500, $e->getMessage());
		}

		if(!$result){
			throw new RestException(404, 'Контакт не найден');
		}

		return $result;
	}

	public function post($name, $surname, $phone)
	{
		try {
			$result = ContactsHelper::create($name, $surname, $phone);
		} catch (Exception $e) {
			throw new RestException(500, $e->getMessage());
		}

		return $result;
	}

	public function put($id, $name = null, $surname = null, $phone = null)
	{
		try {
			$result = ContactsHelper::update($id, $name, $surname, $phone);
		} catch (Exception $e) {
			throw new RestException(500, $e->getMessage());
		}
		if(!$result){
			throw new RestException(400, 'Ошибка в переданных данных или контакт не найден.');
		}

		return $result;
	}

	public function delete($id)
	{
		try {
			$result = ContactsHelper::delete($id);
		} catch (Exception $e) {
			throw new RestException(500, $e->getMessage());
		}
		if(!$result){
			throw new RestException(400, 'Контакт не найден.');
		}

		return $result;
	}
}