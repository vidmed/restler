<?php

namespace src;

use Exception;

class ContactsHelper
{
    private static $fileName = 'contacts.json';

    private static function getFileName()
    {
        return __DIR__ . '/' . self::$fileName;
    }

    private static function readFile()
    {
        $file_name = self::getFileName();
        if (!is_readable($file_name) || ($file = file_get_contents($file_name)) === false) {
            throw new Exception('Unable to read contact file. Make sure it exists and readable.');
        }

        return json_decode($file, true);
    }

    private static function writeFile(array $contacts)
    {
        $file_name = self::getFileName();
        if (
            !is_writable($file_name)
            ||
            ($result = file_put_contents(self::getFileName(), json_encode($contacts, JSON_UNESCAPED_UNICODE))) === false
        ) {
            throw new Exception('Unable to write contact file. Make sure it exists and writable.');
        }

        return true;
    }

    public static function getAll()
    {
        return self::readFile();
    }

    public static function getOne($id)
    {
        $contacts = self::readFile();

        foreach ($contacts as $contact) {
            if ($contact['id'] == $id)
                return $contact;
        }

        return false;
    }

    public static function create($name, $surname, $phone)
    {
        $contacts = self::readFile();
        $id = count($contacts) + 1;

        $new_contact = [
            'id' => $id,
            'name' => $name,
            'surname' => $surname,
            'phone' => $phone,
        ];

        array_push($contacts, $new_contact);

        self::writeFile($contacts);

        return $new_contact;
    }

    public static function update($id, $name = null, $surname = null, $phone = null)
    {
        if (!isset($name) && !isset($surname) && !isset($phone)) {
            return false;
        }

        $contacts = self::readFile();

        foreach ($contacts as $k => $contact) {
            if ($contact['id'] == $id) {
                $new_contact_data = [
                    'id' => $id,
                    'name' => isset($name) ? $name : $contact['name'],
                    'surname' => isset($surname) ? $surname : $contact['surname'],
                    'phone' => isset($phone) ? $phone : $contact['phone'],
                ];
                $contacts[$k] = $new_contact_data;

                self::writeFile($contacts);

                return $new_contact_data;
            }
        }

        return false;
    }

    public static function delete($id)
    {
        $contacts = self::readFile();

        foreach ($contacts as $k => $contact) {
            if ($contact['id'] == $id){
                unset ($contacts[$k]);

                self::writeFile($contacts);

                return true;
            }
        }

        return false;
    }
}